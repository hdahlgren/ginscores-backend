FROM golang:1.10 AS builder

WORKDIR $GOPATH/src/github.com/hdahlgren/ginscores-backend
COPY . ./

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo -o /app .

FROM scratch
COPY --from=builder /app ./
ENTRYPOINT ["./app"]
