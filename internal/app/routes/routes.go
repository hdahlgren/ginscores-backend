package routes

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/hdahlgren/ginscores-backend/configs"
	"github.com/hdahlgren/ginscores-backend/internal/app/handlers"
)

// Initialize : Sets up all the routes for the service.
func Initialize(playerHandler handlers.PlayerHandler, gameSessionHandler handlers.GameSessionHandler) {
	baseRoute := gin.Default()
	baseRoute.Use(CORS())

	routeForPlayer := baseRoute.Group("/api/v1/player")
	{
		routeForPlayer.POST("/create", playerHandler.Create)
		routeForPlayer.Use(mware.ValidateAuthToken)
		{
			routeForPlayer.PUT("/update", playerHandler.Update)
			routeForPlayer.PUT("/change_password", playerHandler.ChangePassword)
			routeForPlayer.GET("/get/:id", playerHandler.Get)
		}
	}

	routeForPlayers := baseRoute.Group("/api/v1/players")
	{
		routeForPlayers.GET("/list_all", playerHandler.ListAll)
	}

	routeForAuthentication := baseRoute.Group("/api/v1/authentication")
	{
		routeForAuthentication.GET("/login", authenticationHandler.Login)
		routeForAuthentication.GET("/validate/:token", authenticationHandler.ValidateToken)
	}

	routeForGameSession := baseRoute.Group("/api/v1/game_session")
	{
		routeForGameSession.Use(mware.ValidateAuthToken)
		{
			routeForGameSession.POST("/create", gameSessionHandler.Create)
			routeForGameSession.POST("/update", gameSessionHandler.Update)
			routeForGameSession.POST("/add_players", gameSessionHandler.AddPlayers)
			routeForGameSession.POST("/remove_players", gameSessionHandler.RemovePlayers)
			routeForGameSession.GET("/get/:id", gameSessionHandler.Get)
		}
	}

	routeForGameSessions := baseRoute.Group("/api/v1/gamesessions")
	{
		routeForGameSessions.Use(mware.ValidateAuthToken)
		{
			routeForGameSessions.GET("/list_for_player/:player_id", gameSessionHandler.ListForPlayer)
		}
	}

	routeForGameRound := baseRoute.Group("/api/v1/gamesessions")
	{
		routeForGameRound.Use(mware.ValidateAuthToken)
		{
			routeForGameRound.POST("/create", gameRoundHandler.Create)
			routeForGameRound.PUT("/add_player_points", gameRoundHandler.AddPlayerPoints)
			routeForGameRound.PUT("/change_player_points", gameRoundHandler.ChangePlayerPoints)
		}
	}

	routeForGameRounds := baseRoute.Group("/api/v1/game_rounds")
	{
		routeForGameRounds.Use(mware.ValidateAuthToken)
		{
			routeForGameRounds.GET("/list_for_game_session/:game_session_id", gameRoundHandler.ListForGameSession)
		}
	}

	baseRoute.Run(fmt.Sprintf(":%v", configs.Port))
}

// CORS : Preflight.
func CORS() gin.HandlerFunc {
	return func(context *gin.Context) {
		context.Writer.Header().Add("Access-Control-Allow-Origin", "*")
		context.Writer.Header().Set("Access-Control-Max-Age", "86400")
		context.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
		context.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		context.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length")
		context.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		if context.Request.Method == "OPTIONS" {
			context.AbortWithStatus(200)
		} else {
			context.Next()
		}
	}
}
