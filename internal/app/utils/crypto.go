package utils

import (
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
)

func HashString(text string) string {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(text), 10)
	hash := string(bytes)
	return hash
}

func CompareHashedString(text string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(text))
	return err == nil
}

func GetNewToken() string {
	u, _ := uuid.NewV4()
	return u.String()
}
