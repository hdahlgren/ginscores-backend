package models

// AuthenticatedPlayer : AuthenticatedPlayer info
type AuthenticatedPlayer struct {
	Player    Player
	AuthToken string
}
