package models

// PlayerPoint : Holds all data related to the GameRound
type PlayerPoint struct {
	BaseModel
	GameRoundID uint
	PlayerID    uint
	Points      int
}
