package models

// GameRound : Holds all data related to the GameRound
type GameRound struct {
	BaseModel
	GameSessionID uint
	Points        []PlayerPoint
}
