package models

import (
	"encoding/json"
)

// Player : Holds all data related to the player object.
type Player struct {
	BaseModel
	Email    string
	Password string
	Nickname string
}

// MarshalJSON : Used when transmitting object
func (player Player) MarshalJSON() ([]byte, error) {
	var tmp struct {
		ID       uint
		Nickname string
		Email    string
	}
	tmp.ID = player.ID
	tmp.Nickname = player.Nickname
	tmp.Email = player.Email

	return json.Marshal(&tmp)
}
