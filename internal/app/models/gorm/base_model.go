package models

import "time"

// BaseModel : Holds basic GORM stuff
type BaseModel struct {
	ID   uint `gorm:"primary_key"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-" sql:"index"`
}
