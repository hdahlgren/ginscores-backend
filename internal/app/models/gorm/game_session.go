package models

import "time"

// GameSession : Holds all data related to the gamesession object.
type GameSession struct {
	BaseModel
	Host        uint
	Players     []Player
	Description string
	EndedAt     *time.Time
}
