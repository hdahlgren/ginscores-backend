package models

import "time"

// AuthTokenInfo : AuthTokenInfo description
type AuthTokenInfo struct {
	AuthToken   string    `db:"Token"`
	PlayerID    int64     `db:"PlayerId" json:"-"`
	IP          string    `db:"IP"`
	DateCreated time.Time `db:"DateCreated" json:"-"`
	DateValidTo time.Time `db:"DateValidTo" json:"-"`
}
