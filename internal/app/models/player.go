package models

// Player : Player info
type Player struct {
	PlayerID uint
	Email    string
	Password string
	Nickname string
}
