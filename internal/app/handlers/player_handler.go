package handlers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/hdahlgren/ginscores-backend/internal/app/models"
	"gitlab.com/hdahlgren/ginscores-backend/internal/app/repositories"
)

// PlayerHandler : Abstraction around player handler.
type PlayerHandler struct {
	playerRepository repositories.PlayerRepository
}

// Initialize : Set up the player handler.
func (ph *PlayerHandler) Initialize() {
	log.Printf("initializing player handler...")

	// Set a reference to the player repo, and intialize.
	ph.playerRepository = repositories.PlayerRepository{}
	ph.playerRepository.Initialize()
}

// List : Returns an array list of all players, should accept any filter and pagination parameters.
func (ph *PlayerHandler) List(ctx *gin.Context) {
	var players []models.Player
	ph.playerRepository.Find(&players)

	if len(players) <= 0 {
		ctx.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "no players found"})
		return
	}

	ctx.JSON(http.StatusOK, players)
}

// Show : Retrieve exactly one player, referenced by ID.
func (ph *PlayerHandler) Show(ctx *gin.Context) {
	player := ph.findPlayer(ctx.Param("id"), ctx)

	// Player was not found.
	if player.ID == 0 {
		ctx.JSON(http.StatusNotFound, HTTPResponse{
			Code:    http.StatusNotFound,
			Message: "no player found by id",
		})
		return
	}

	// Player found, send the resposne.
	ctx.JSON(http.StatusOK, player)
}

// Login : Get player based on credentials and return authtoken
func (ph *PlayerHandler) Login(ctx *gin.Context) {
	email, password, _ := ctx.Request.BasicAuth()

	var player models.Player
	ph.playerRepository.FirstByEmail(&player, email)

	// Player was not found.
	if player.ID == 0 {
		ctx.JSON(http.StatusNotFound, HTTPResponse{
			Code:    http.StatusNotFound,
			Message: "no player found by email",
		})
		return
	}

	log.Println("Password", player.Password, "=", password)

	//if !utils.CompareHashedString(player.Password, password) {
	if player.Password != password {
		ctx.JSON(http.StatusNotFound, HTTPResponse{
			Code:    http.StatusNotFound,
			Message: "invalid credentials",
		})
		return
	}

	// Player found, send the resposne.
	ctx.JSON(http.StatusOK, player)
}

// Create : Create a new player in the database, using the params passed in, then return the player id.
func (ph *PlayerHandler) Create(ctx *gin.Context) {
	var player models.Player
	ctx.BindJSON(&player)

	ph.playerRepository.Save(&player)

	if ph.playerRepository.IsValid == false {
		ctx.JSON(http.StatusUnprocessableEntity, UnprocessableEntity{
			Code:    http.StatusUnprocessableEntity,
			Message: "failed to create player",
			Errors:  ph.playerRepository.ValidationErrors,
		})

		return
	}

	if player.ID != 0 {
		ctx.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": player})
	} else {
		ctx.JSON(http.StatusNoContent, HTTPResponse{
			Code:    http.StatusNoContent,
			Message: "no player could be created",
		})
	}
}

// Update : Takes care of updating a stored player object.
func (ph *PlayerHandler) Update(ctx *gin.Context) {
	player := ph.findPlayer(ctx.Param("id"), ctx)

	// Player was not found.
	if player.ID == 0 {
		ctx.JSON(http.StatusNotFound, HTTPResponse{
			Code:    http.StatusNotFound,
			Message: "no player found by id",
		})

		return
	}

	ctx.BindJSON(&player)
	ph.playerRepository.Save(&player)

	if ph.playerRepository.IsValid == false {
		ctx.JSON(http.StatusUnprocessableEntity, UnprocessableEntity{
			Code:    http.StatusUnprocessableEntity,
			Message: "failed to update player",
			Errors:  ph.playerRepository.ValidationErrors,
		})

		return
	}

	ctx.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": player})
}

// Destroy : Removes the player from API visibility.
func (ph *PlayerHandler) Destroy(ctx *gin.Context) {
	player := ph.findPlayer(ctx.Param("id"), ctx)

	// Player was not found.
	if player.ID == 0 {
		ctx.JSON(http.StatusNotFound, HTTPResponse{
			Code:    http.StatusNotFound,
			Message: "no player found by id",
		})

		return
	}

	ctx.JSON(http.StatusOK, HTTPResponse{
		Code:    http.StatusOK,
		Message: "player successfully deleted",
	})
}

// HELPERS THAT ALLOW US TO BE MORE DRY.
func (ph *PlayerHandler) findPlayer(playerID string, ctx *gin.Context) models.Player {
	var player models.Player
	ph.playerRepository.First(&player, playerID)
	return player
}
