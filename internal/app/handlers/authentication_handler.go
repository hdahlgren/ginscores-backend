package handlers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/hdahlgren/ginscores-backend/internal/app/models"
	"gitlab.com/hdahlgren/ginscores-backend/internal/app/repositories"
	"gitlab.com/hdahlgren/ginscores-backend/internal/app/utils"
)

// AuthenticationHandler : Abstraction around handler.
type AuthenticationHandler struct {
	authenticationRepository repositories.AuthenticationRepository
	playerRepository         repositories.PlayerRepository
}

// Initialize : Set up the handler.
func (handler *AuthenticationHandler) Initialize() {
	log.Printf("initializing game session handler...")

	// Set a reference to the repo, and intialize.
	handler.authenticationRepository = repositories.AuthenticationRepository{}
	handler.authenticationRepository.Initialize()

	handler.playerRepository = repositories.PlayerRepository{}
	handler.playerRepository.Initialize()
}

// Login : Login
func (handler *AuthenticationHandler) Login(ctx *gin.Context) {
	// Get data from basic auth header
	email, password, _ := ctx.Request.BasicAuth()

	// Get player based from db by email
	var player models.Player
	handler.playerRepository.GetPlayerByEmail(&player, email)

	// Player was not found.
	if player.PlayerID == 0 {
		ctx.JSON(http.StatusNotFound, HTTPResponse{
			Code:    http.StatusNotFound,
			Message: "no game session found by id",
		})
		return
	}

	// Check that the player entered the correct password
	//if !utils.CompareHashedString(player.Password, password) {
	if player.Password != password {
		ctx.JSON(http.StatusNotFound, HTTPResponse{
			Code:    http.StatusUnauthorized,
			Message: "invalid credentials",
		})
		return
	}

	// Create new authtoken
	authToken := utils.GetNewToken()
	handler.authenticationRepository.SaveAuthToken(&authToken, player.PlayerID)

	// Create response
	authenticatedPlayer := AuthenticatedPlayer{
		Player:    player,
		AuthToken: authToken,
	}

	// Authtoken set
	ctx.JSON(http.StatusOK, authenticatedPlayer)
}
