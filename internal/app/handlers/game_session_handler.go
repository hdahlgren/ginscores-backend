package handlers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/hdahlgren/ginscores-backend/internal/app/models"
	"gitlab.com/hdahlgren/ginscores-backend/internal/app/repositories"
)

// GameSessionHandler : Abstraction around player handler.
type GameSessionHandler struct {
	gameSessionRepository repositories.GameSessionRepository
}

// Initialize : Set up the player handler.
func (handler *GameSessionHandler) Initialize() {
	log.Printf("initializing game session handler...")

	// Set a reference to the player repo, and intialize.
	handler.gameSessionRepository = repositories.GameSessionRepository{}
	handler.gameSessionRepository.Initialize()
}

// List : Returns an array list of all players, should accept any filter and pagination parameters.
func (handler *GameSessionHandler) List(ctx *gin.Context) {
	var gameSessions []models.GameSession
	handler.gameSessionRepository.Find(&gameSessions)

	if len(gameSessions) <= 0 {
		ctx.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "no game sessions found"})
		return
	}

	ctx.JSON(http.StatusOK, gameSessions)
}

// Show : Retrieve exactly one player, referenced by ID.
func (handler *GameSessionHandler) Show(ctx *gin.Context) {
	player := handler.findPlayer(ctx.Param("id"), ctx)

	// Player was not found.
	if player.ID == 0 {
		ctx.JSON(http.StatusNotFound, HTTPResponse{
			Code:    http.StatusNotFound,
			Message: "no game session found by id",
		})
		return
	}

	// Player found, send the resposne.
	ctx.JSON(http.StatusOK, player)
}

// Create : Create a new player in the database, using the params passed in, then return the player id.
func (handler *GameSessionHandler) Create(ctx *gin.Context) {
	var gameSession models.GameSession
	ctx.BindJSON(&gameSession)

	handler.gameSessionRepository.Save(&gameSession)

	if handler.gameSessionRepository.IsValid == false {
		ctx.JSON(http.StatusUnprocessableEntity, UnprocessableEntity{
			Code:    http.StatusUnprocessableEntity,
			Message: "failed to create game session",
			Errors:  handler.gameSessionRepository.ValidationErrors,
		})

		return
	}

	if gameSession.ID != 0 {
		ctx.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": gameSession})
	} else {
		ctx.JSON(http.StatusNoContent, HTTPResponse{
			Code:    http.StatusNoContent,
			Message: "no game session could be created",
		})
	}
}

// AddPlayersToGameSession : Retrieve exactly one player, referenced by ID.
func (handler *GameSessionHandler) AddPlayersToGameSession(ctx *gin.Context) {
	player := handler.findPlayer(ctx.Param("id"), ctx)

	// Player was not found.
	if player.ID == 0 {
		ctx.JSON(http.StatusNotFound, HTTPResponse{
			Code:    http.StatusNotFound,
			Message: "no game session found by id",
		})
		return
	}

	// Player found, send the resposne.
	ctx.JSON(http.StatusOK, player)
}

// HELPERS THAT ALLOW US TO BE MORE DRY.
func (handler *GameSessionHandler) findPlayer(gameSessionID string, ctx *gin.Context) models.GameSession {
	var gameSession models.GameSession
	handler.gameSessionRepository.First(&gameSession, gameSessionID)
	return gameSession
}
