package handlers

// HTTPResponse : Represent a simple/generic HTTP response.
// swagger:response HTTPResponse
type HTTPResponse struct {
	Code    int
	Message string
}

// HTTPResponseWithID : Represent a simple/generic HTTP response with entity ID.
// swagger:response HTTPResponseWithID
type HTTPResponseWithID struct {
	Code    int
	Message string
	ID      uint
}

// UnprocessableEntity : Represents a 422 Unprocessable Entity response.
// swagger:response UnprocessableEntity
type UnprocessableEntity struct {
	Code    int
	Message string
	Errors  []error
}
