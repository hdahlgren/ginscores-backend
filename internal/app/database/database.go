package database

import (
	"database/sql"
	"log"
	"time"

	"gitlab.com/hdahlgren/ginscores-backend/configs"

	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/jmoiron/sqlx"
)

// DBManager : Abstraction around the database, implements needed data and operations.
type DBManager struct {
	db  *sqlx.DB
	err error
}

// Initialize : Sets up the abstaction layer.
func (dbm *DBManager) Initialize() {
	log.Println("Opening new connection")
	db, err := sqlx.Open("mysql", configs.DBConnectionString)
	if err != nil {
		log.Fatal("failed to connect database", err)
	}

	db.DB.SetConnMaxLifetime(time.Minute * 1)
	db.DB.SetMaxIdleConns(configs.MaxIdleConns)
	db.DB.SetMaxOpenConns(configs.MaxOpenConns)
}

// ExecuteQuery : ExecuteQuery
func (dbm *DBManager) ExecuteQuery(query string, args ...interface{}) interface{} {
	_, err := dbm.db.Exec(query, args...)

	if err != nil && err != sql.ErrNoRows {
		log.Print("Error executing no row query", err)
		return err
	}

	return nil
}

// ExecuteBindingSingleQuery : ExecuteBindingSingleQuery
func (dbm *DBManager) ExecuteBindingSingleQuery(bindingInterface interface{}, query string, args ...interface{}) interface{} {
	err := dbm.db.QueryRowx(query, args...).StructScan(bindingInterface)

	if err != nil && err == sql.ErrNoRows {
		log.Print("Error executing single row query and no rows found", err)
		return err
	}

	if err != nil {
		log.Print("Error executing single row query", err)
		return err
	}
	return nil
}

// ExecuteBindingMultiQuery : ExecuteBindingMultiQuery
func (dbm *DBManager) ExecuteBindingMultiQuery(bindingInterface interface{}, query string, args ...interface{}) interface{} {
	rows, err := dbm.db.Queryx(query, args...)

	if err != nil && err == sql.ErrNoRows {
		rows.Close()
		log.Print("Error executing multirow query and no rows found", err)
		return err
	}

	if err != nil {
		rows.Close()
		log.Print("Error executing multirow query (Queryx)", err)
		return err
	}

	err = sqlx.StructScan(rows, bindingInterface)

	if err != nil {
		log.Print("Error executing multirow query (StructScan)", err)
		return err
	}

	return nil
}
