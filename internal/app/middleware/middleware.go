package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/hdahlgren/ginscores-backend/internal/app/repositories"
)

// ValidateAuthToken checks that an incoming request has a valid authToken in the header called 'authtoken'
func ValidateAuthToken(ctx *gin.Context) {
	authToken := ctx.Request.Header.Get("authtoken")

	// Set a reference to the player repo, and intialize.
	authenticationRepository := repositories.AuthenticationRepository{}
	authenticationRepository.Initialize()

	if !authenticationRepository.IsValidAuthToken(authToken) {

		var response struct {
			Success bool
			Data    interface{}
			Errors  struct {
				Code    int
				Message string
			}
		}
		response.Success = false
		response.Data = nil
		response.Errors.Code = 666
		response.Errors.Message = "Invalid AuthToken"

		ctx.JSON(http.StatusUnauthorized, response)
		ctx.Abort()
		return
	}
	ctx.Next()
}
