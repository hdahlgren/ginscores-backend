package repositories

import (
	"log"
	"time"

	"github.com/hdahlgren/ginscores-backend/internal/app/database"
	"gitlab.com/hdahlgren/ginscores-backend/internal/app/models"
)

// AuthenticationRepository ...
type AuthenticationRepository struct {
	BaseRepository
}

// Initialize ...
func (repo *AuthenticationRepository) Initialize() {
	log.Printf("initializing player repository...")

	// Set a reference to the database manager, and intialize.
	repo.dbm = database.DBManager{}
	repo.dbm.Initialize()
}

// SaveAuthToken : SaveAuthToken
func (repo *AuthenticationRepository) SaveAuthToken(success *bool, authTokenInfo models.AuthTokenInfo) bool {
	err := repo.dbm.ExecuteQuery(`INSERT INTO authtokens (authtoken, created_at, player_id, valid_to, ip) VALUES (?, ?, ?, ?, ?)`, authTokenInfo.AuthToken, time.Now().UTC(), authTokenInfo.PlayerId, time.Now().UTC().Add(time.Minute*10), authTokenInfo.IP)
	if err != nil {
		log.Println("Authtoken could not be saved. // ", err)
		return false
	}
	return true
}

// GetAuthTokenInfo : GetAuthTokenInfo
func (repo *AuthenticationRepository) GetAuthTokenInfo(authTokenInfo *models.AuthTokenInfo, authToken string) {
	err := repo.dbm.ExecuteBindingSingleQuery(&authTokenInfo, `SELECT * FROM authtokens WHERE authtoken = ?`, authToken)
	if (err != nil) || (authTokenInfo.PlayerId == 0) {
		log.Println("AuthTokenInfo not found [ authtoken:", authToken, "]  //  ", err)
	}
}

// InvalidateToken : InvalidateToken
func (repo *AuthenticationRepository) InvalidateToken(authToken string) bool {
	err := repo.dbm.ExecuteQuery(`UPDAE authtokens SET valid_to = ? WHERE authtoken = ?`, time.Now().UTC(), authToken)
	if err != nil {
		log.Println("Authtoken could not be invalidated. // ", err)
		return false
	}
	return true
}
