package repositories

import (
	"log"

	"github.com/hdahlgren/ginscores-backend/internal/app/database"
	"github.com/hdahlgren/ginscores-backend/internal/app/models/gorm"
)

// PlayerRepository ...
type PlayerRepository struct {
	BaseRepository
}

// Initialize ...
func (repo *PlayerRepository) Initialize() {
	log.Printf("initializing player repository...")

	// Set a reference to the database manager, and intialize.
	repo.dbm = database.DBManager{}
	repo.dbm.Initialize()

	// Automigrate
	repo.dbm.Schema.AutoMigrate(&models.Player{})
	repo.dbm.Schema.Model(&models.Player{}).AddUniqueIndex("idx_username", "username")
	repo.dbm.Schema.Model(&models.Player{}).AddUniqueIndex("idx_email", "email")
}

// Find : Gets a list of players from the database.
// TODO: Needs proper error checking.
func (repo *PlayerRepository) Find(players *[]models.Player) {
	errors := repo.dbm.Schema.Set("gorm:auto_preload", true).Find(&players).GetErrors()
	repo.validate(errors)
}

// First : Gets a single player from the database.
func (repo *PlayerRepository) First(player *models.Player, playerID string) {
	errors := repo.dbm.Schema.First(&player, playerID).GetErrors()
	repo.validate(errors)
}

// FirstByEmail : Gets a single player from the database.
func (repo *PlayerRepository) FirstByEmail(player *models.Player, email string) {
	errors := repo.dbm.Schema.Where("email = ?", email).First(&player).GetErrors()
	repo.validate(errors)
}

// Save : Stores a new player into the schema.
func (repo *PlayerRepository) Save(player *models.Player) {
	errors := repo.dbm.Schema.Save(&player).GetErrors()
	repo.validate(errors)
}

// Update : Updates a player
func (repo *PlayerRepository) Update(player *models.Player, newPlayerData models.Player) {
	errors := repo.dbm.Schema.Update(&player, newPlayerData).GetErrors()
	repo.validate(errors)
}

// Delete : Removes the player from visibility, should not delete the data, only hide it.
func (repo *PlayerRepository) Delete(player *models.Player) {
	errors := repo.dbm.Schema.Delete(&player).GetErrors()
	repo.validate(errors)
}
