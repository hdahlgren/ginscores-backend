package repositories

import (
	"log"

	"github.com/hdahlgren/ginscores-backend/internal/app/database"
	"github.com/hdahlgren/ginscores-backend/internal/app/models/gorm"
)

// GameSessionRepository ...
type GameSessionRepository struct {
	BaseRepository
}

// Initialize ...
func (repo *GameSessionRepository) Initialize() {
	log.Printf("initializing game repository...")

	// Set a reference to the database manager, and intialize.
	repo.dbm = database.DBManager{}
	repo.dbm.Initialize()

	// Automigrate
	repo.dbm.Schema.AutoMigrate(&models.GameSession{})
	repo.dbm.Schema.AutoMigrate(&models.GameRound{})
	repo.dbm.Schema.AutoMigrate(&models.GameRoundPoint{})
}

// Find : Gets a list of players from the database.
func (repo *GameSessionRepository) Find(gameSessions *[]models.GameSession) {
	errors := repo.dbm.Schema.Set("gorm:auto_preload", true).Find(&gameSessions).GetErrors()
	repo.validate(errors)
}

// First : Gets a single gameSession from the database.
func (repo *GameSessionRepository) First(gameSession *models.GameSession, gameSessionID string) {
	errors := repo.dbm.Schema.First(&gameSession, gameSessionID).GetErrors()
	repo.validate(errors)
}

// Save : Stores a new player into the schema.
func (repo *GameSessionRepository) Save(gameSession *models.GameSession) {
	errors := repo.dbm.Schema.Save(&gameSession).GetErrors()
	repo.validate(errors)
}

// Update : Updates a player
func (repo *GameSessionRepository) Update(gameSession *models.GameSession, newData models.GameSession) {
	errors := repo.dbm.Schema.Update(&gameSession, newData).GetErrors()
	repo.validate(errors)
}

// Delete : Removes the player from visibility, should not delete the data, only hide it.
func (repo *GameSessionRepository) Delete(gameSession *models.GameSession) {
	errors := repo.dbm.Schema.Delete(&gameSession).GetErrors()
	repo.validate(errors)
}
