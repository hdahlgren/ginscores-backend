package repositories

import (
	"github.com/hdahlgren/ginscores-backend/internal/app/database"
)

// BaseRepository ...
type BaseRepository struct {
	dbm              database.DBManager
	ValidationErrors []error
	IsValid          bool
}

// validate : Handle validation errors.
func (repo *BaseRepository) validate(errors []error) {
	if len(errors) > 0 {
		repo.ValidationErrors = errors
		repo.IsValid = false
		return
	}

	repo.IsValid = true
}
