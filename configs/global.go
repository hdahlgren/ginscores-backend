package configs

import (
	"log"
	"os"
	"strconv"
)

var (
	// AppName : AppName
	AppName = getEnv("APP_NAME", "Gin Scores v0.1")
	// Port : Port
	Port = getEnv("PORT", "7001")
	// DBConnectionString : DBConnectionString
	DBConnectionString = getEnv("DATABASE_URL", "root:root@tcp(127.0.0.1:3306)/ginscores?parseTime=true")
	// MaxIdleConns : MaxIdleConns
	MaxIdleConns = getIntEnv("MAX_IDLE_CONNS", 10)
	// MaxOpenConns : MaxOpenConns
	MaxOpenConns = getIntEnv("MAX_OPEN_CONNS", 90)
)

// getEnv gets an environment variable with fallback if not set.
// It returns string value of key passed from environment variables
func getEnv(key, fallback string) string {
	value := os.Getenv(key)

	if len(value) == 0 {
		log.Printf("Environment variable %s was not found. Using default %s instead!", key, fallback)
		return fallback
	}

	return value
}

// getIntEnv gets an environment variable with fallback if not set.
// It returns int value of key passed from environment variables
func getIntEnv(key string, fallback int) int {
	value := os.Getenv(key)
	if len(value) == 0 {
		log.Printf("Environment variable %s was not found. Using default %v instead!", key, fallback)
		return fallback
	}

	val, err := strconv.Atoi(value)
	if err != nil {
		log.Printf("Environment variable %s was not found. Using default %v instead!", key, fallback)
		return fallback
	}

	return val
}

// getBoolEnv gets an environment variable with fallback if not set.
// It returns bool value of key passed from environment variables
func getBoolEnv(key string, fallback bool) bool {
	value := os.Getenv(key)
	if len(value) == 0 {
		log.Printf("Environment variable %s was not found. Using default %v instead!", key, fallback)
		return fallback
	}

	val, err := strconv.ParseBool(value)
	if err != nil {
		log.Printf("Environment variable %s was not found. Using default %v instead!", key, fallback)
		return fallback
	}

	return val
}
