package main

import (
	"log"
	"os"

	"github.com/hdahlgren/ginscores-backend/internal/app/handlers"
	"github.com/hdahlgren/ginscores-backend/internal/app/routes"
)

func main() {
	// Initialize a new logger.
	log.Println("starting Gin Score service...")

	// Initialse the global variables
	os.Setenv("DB_URL", "127.0.0.1")
	os.Setenv("DB_USER", "root")
	os.Setenv("DB_PASS", "root")
	os.Setenv("PORT", "7001")

	// Initialize a handlers
	playerHandler := handlers.PlayerHandler{}
	playerHandler.Initialize()

	gameSessionHandler := handlers.GameSessionHandler{}
	gameSessionHandler.Initialize()

	// Initializes all the routes for the service, pass in the player handler abstraction.
	routes.Initialize(playerHandler, gameSessionHandler)
}
